console.log('Hello World');

// [SECTION] Array Methods

    // JS has built-in functions and methods for arrays. This allow us to manipulate and access array items.

// [SECTION] Mutators Methods

    // Mutator methods are functions that 'mutate' or change an array after they're created.

    let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

        // [SUB-SECTION] push()

        // ADDS an element in the end of an array AND returns the array's length.

            console.log('Current array');
            console.log(fruits);

            let fruitsLength = fruits.push('Mango');
            console.log('Mutated array from push method:');
            console.log(fruits);            /* Result ---> (5) ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango'] */
            console.log(fruitsLength);      /* Result ---> 5 */

            // Adding multiple elements to an array
            fruits.push('Avocado', 'Guava');
            console.log('Mutated array from push method:');
            console.log(fruits);            /* Result ---> (7) ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado', 'Guava'] */

        // [SUB-SECTION] pop()

        // REMOVES the last element in an array AND returns the removed element

        /* 
            Syntax:
                arrayName.pop();
        */

            let removedFruit = fruits.pop();
            console.log(removedFruit);      /* Result ---> Guava */
            console.log('Mutated array from pop method:');
            console.log(fruits);            /* Result ---> (6) ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'] */

        // [SUB-SECTION] unshift()

        // ADDS one or more elements at the beginning of an array.

        /* 
            Syntax:
                arrayName.unshift();
        */

            fruits.unshift('Lime', 'Banana');
            console.log('Mutated array from unshift method:');
            console.log(fruits);            /* Result ---> (8) ['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'] */

        // [SUB-SECTION] shift()

        // REMOVES an element at the beginning of an array AND returns that removed element.

        /* 
            Syntax:
                arrayName.shift();
        */

            let anotherFruit = fruits.shift();
            console.log(anotherFruit);      /* Result ---> Lime */
            console.log('Mutated array from shift method:');
            console.log(fruits);            /* Result ---> (7) ['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'] */

        // [SUB-SECTION] splice()

        // Simultaneously removes elements from a specified index number and adds elements.

            fruits.splice(1, 2, 'Lime', 'Cherry');
            console.log('Mutated array from splice method:');
            console.log(fruits);            /* Result ---> (7) ['Banana', 'Lime', 'Cherry', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado'] */

        // [SUB-SECTION] sort()

        // Rearranges the array elements in alhanumeric order.

        /* 
            Syntax:
                arrayName.sort();
        */

            fruits.sort();
            console.log('Mutated array from sort method:');
            console.log(fruits);            /* Result ---> (7) ['Avocado', 'Banana', 'Cherry', 'Dragon Fruit', 'Kiwi', 'Lime', 'Mango'] */

        // [SUB-SECTION] reverse()

        // Reverses the order of array elements.

        /* 
            Syntax:
                arrayName.reverse();
        */

            fruits.reverse();
            console.log('Mutated array from reverse method:');
            console.log(fruits);            /* Result ---> (7) ['Mango', 'Lime', 'Kiwi', 'Dragon Fruit', 'Cherry', 'Banana', 'Avocado'] */

// [SECTION] Non-Mutators Methods

    console.log('Non-Mutators Methids');

    // Non-mutator methods are functions that do not modify or change an array after they're created.

    let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'FR', 'DE', 'PH'];

    console.log(countries);

    // [SUB-SECTION] indexOf()

    // Returns the index number of the first matching element found in an array.

        let firstIndex = countries.indexOf('PH');
        console.log('Result of indexOf Method: ' + firstIndex);     /* Result of indexOf Method: 1 */

        let invalidCountry = countries.indexOf('CH');
        console.log('Result of indexOf Method: ' + invalidCountry); /* Result of indexOf Method: -1 */
    
    // [SUB-SECTION] lastIndexOf()

    // Returns the index number of the last matching element found in an array.

        // Getting the index number starting from the last element.
        let lastIndex = countries.lastIndexOf('PH');
        console.log('Result of lastIndexOf method: ' + lastIndex);       /* Result of lastIndexOf method: 7 */

        // Getting the index number starting from a specified index
        let lastIndexStart = countries.lastIndexOf('PH', 6);
        console.log('Result of lastIndexOf method: ' + lastIndexStart);  /* Result of lastIndexOf method: 1 */

    // [SUB-SECTION] slice()

    // Portions/slices elements from an array AND returns a new array.

        let slicedArrayA = countries.slice(2);
        console.log('Result from slice method: ');
        console.log(slicedArrayA);                                      /* Result ---> ['CAN', 'SG', 'TH', 'FR', 'DE', 'PH'] */
        console.log(countries);

        // Slicing off elements from a specified index to another element
        let slicedArrayB = countries.slice(2, 6);                       /* 'index 2 to 6' */
        console.log('Result from slice method: ');
        console.log(slicedArrayB);                                      /* Result ---> (4) ['CAN', 'SG', 'TH', 'FR'] */

        // Slicing off elements starting from the last element
        let slicedArrayC = countries.slice(-4);
        console.log('Result from slice method: ');
        console.log(slicedArrayC);                                      /* Result ---> (4) ['TH', 'FR', 'DE', 'PH'] */

    // [SUB-SECTION] toString()

    // Returns an array as a string separated by commas

        let stringArray = countries.toString();
        console.log('Result from toString method: ');
        console.log(stringArray);                                       /* Result ---> US,PH,CAN,SG,TH,FR,DE,PH */

    // [SUB-SECTION] concat()

    // Combines two arrays and returns the combined result.

        let tasksArrayA = ['drink html', 'eat javascript'];
        let tasksArrayB = ['inhale css', 'breathe sass'];
        let tasksArrayC = ['get git', 'be node'];

            let tasks = tasksArrayA.concat(tasksArrayB);
            console.log('Result of concat method: ');
            console.log(tasks);                                         /* Result ---> (4) ['drink html', 'eat javascript', 'inhale css', 'breathe sass'] */

            // Combining multiple arrays
            console.log('Result from concat method: ');
            let  allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
            console.log(allTasks);                                      /* Result ---> (8) ['drink html', 'eat javascript', 'drink html', 'eat javascript', 'inhale css', 'breathe sass', 'get git', 'be node'] */

            // Combining arrays with elements
            let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
            console.log('Result from concat method: ');
            console.log(combinedTasks);                                 /* Result ---> (4) ['drink html', 'eat javascript', 'smell express', 'throw react'] */

    // [SUB-SECTION] join()

    // Returns an array as a string separated by specified separator string.

        let users = ['John', 'Jane', 'Joe', 'Robert'];

        console.log(users.join());                                      /* John,Jane,Joe,Robert */
        console.log(users.join(''));                                    /* JohnJaneJoeRobert */
        console.log(users.join('-'));                                   /* John-Jane-Joe-Robert */

// [SECTION] Iterations Methods

    // Iterations methods are loops designed to perform repetitive tasks on arrays

    // [SUB-SECTION] forEach()

        // Similar to a for loop that iterates on each array element

        /* 
            Syntax:
                arrayName.forEach(function(indivElement){
                    statement
                })
        */

            allTasks.forEach(function(task1) {                          
                console.log(task1);
            });
            
            /* 
                drink html
                eat javascript
                inhale css
                breathe sass
                get git
                be node
            */

            // Using forEach with conditional stements
                let filteredTask = [];

        // [SUB-SECTION] Looping through all array items.

            // Creating a separate variable to store results of array iteration methods are also good proactive to avoid confusion by modifying the original array.

                allTasks.forEach(function (task) {

                    // If the element/string's length is greater than 10 characters
                    if (task.length > 10) {

                        // Behavior will be to add these elements in the filteredTasks array
                        filteredTask.push(task);
                    };
                });

                console.log('Result of filtered tasks: ');
                console.log(filteredTask);

        // [SUB-SECTION] map()

            // Iterates on each element AND returns new array with different values depending on the result of the function's operation.

                let numbers = [1, 2, 3, 4, 5];

                let numbersMap = numbers.map(function (number) {
                    return number * number;
                });

                console.log('Original Array: ');
                console.log(numbers);
                console.log('Result of map method: ');
                console.log(numbersMap);

        // [SUB-SECTION] map() vs forEach()

            let numberForEach = numbers.forEach(function (number) {
                return numbers * 3;
            });

            console.log(numberForEach);     /* undefined */

            // forEach(), loops over all items in the array does mao(), but forEach() does not return a new array.

        // [SUB-SECTION] every()

            // Checks if all elements in an array meet the given condition

            let allValid = numbers.every(function (number) {
                return (number < 10);
            });

            console.log('Result of every method: ');
            console.log(allValid);

        // [SUB-SECTION] some()

            // Checks if at least one element in the array meets the given condition.

            let someValid = numbers.some(function (number) {
                return (numbers < 2);
            });

            console.log('Result of some method: ');
            console.log(someValid);

            // Combining the returned result from the every/some method may be used in other statements to perfrom consecutive results.

            if (allValid) {
                console.log('Some numbers in the array are greater than 2');
            };

        // [SUB-SECTIOB] filter()

            // Returns new array that contains elements which meets the given condition.

            let filterValid = numbers.filter(function (number) {
                return (number < 3);
            });

            console.log('Result of filter method: ');
            console.log(filterValid);

            // No element found

            let nothingFound = numbers.filter(function (number) {
                return (number = 0);
            });

            console.log('Result of filter method: ');
            console.log(nothingFound);

            // Filtering using forEach

            let filteredNumbers = [];

            numbers.forEach(function (number) {
                if (number < 3) {
                    filteredNumbers.push(number);
                };
            });

            console.log('Result of filter method: ');
            console.log(filteredNumbers);

        // [SUB-SECTION] includes()

            // includes() method checks if the argument passed can be found in the array.

            let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

            let productFound1 = products.includes('Mouse');
            console.log(productFound1);

            let productFound2 = products.includes('Headset');
            console.log(productFound2);

            /* 
                - Methods can be 'chained' using them one after another
                - The result of the first method is used on the second method until all 'chained' methods have been resolved.
                - How chaing resolves in our example:
                    1. The 'product' element will be convert into all lowercase letters.
                    2. The resulting lowercase string is used in the 'includes' method.
            */

            let filteredProducts = products.filter(function (product) {
                return product.toLocaleLowerCase().includes('a');
            });

            console.log(filteredProducts);

        // [SUB-SECTION] reduce()

            // Evaluates elements from left to right and return/reduces the array into a single value.
            /* 
                1. The first/result element in the array is stored in the 'accumulator' parameter.
                2. The second/next element in the array is stored
                3. An operation is performed on the two elements
                4. The loop repeats step 1-3 until all elements have been worked on.
            */

            let iteration = 0;

                console.log(numbers);

                let numbers1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

            let reducedArray = numbers1.reduce(function (x, y) {
                console.warn('current iteration: ' + ++iteration);
                console.log('accumulator: ' + x);
                console.log('currentValue: ' + y);

                return x + y;
            });

            console.log('Result of reduce method: ' + reducedArray);

            // Reducing String Arrays

            let list = ['Hello', 'Again', 'World'];

            let reducedJoin = list.reduce(function (x, y) {
                return x + ' ' + y;
            });

            console.log('Result of reduced method: ' + reducedJoin);